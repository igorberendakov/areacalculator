﻿using AreaCalculator;
using AreaCalculator.Abstractions;
using System.Text;

Console.OutputEncoding = Encoding.UTF8;

IFigure[] figures =
{
    new Circle(10),
    new Square(4),
    new Triangle(5, 3, 4)
};

foreach (IFigure figure in figures)
{
    string figureName = figure.GetType().Name.ToLower();
    string figureType = string.Empty;

    if (figure is Triangle triangle)
    {
        figureType = string.Format("{0}", triangle.IsRight ? "right " : "");
    }

    Console.WriteLine("Figure is {0}{1}", figureType, figureName);
    Console.WriteLine("Figure area = {0}", figure.Area);
}

/// <summary>
/// Пример добавления фигуры.
/// </summary>
public class Square : FigureBase
{
    private double _side;

    /// <summary>
    /// Сторона квадрата.
    /// </summary>
    public double Side
    {
        get
        {
            return _side;
        }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("Сторона квадрата не может быть меньше нуля.");
            }

            _side = value;
        }
    }

    /// <summary>
    /// Создание экземпляра квадрата по стороне.
    /// </summary>
    /// <param name="side">Сторона квадрата.</param>
    public Square(double side = default)
    {
        Side = side;
    }

    /// <summary>
    /// Метод расчета площади квадрата.
    /// </summary>
    /// <returns></returns>
    protected override double CalculateFigureArea()
    {
        return Math.Pow(Side, 2);
    }
}