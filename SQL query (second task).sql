IF NOT EXISTS (SELECT * FROM sysobjects WHERE NAME='Products' and xtype='U')
    CREATE TABLE Products (
        Id INT IDENTITY NOT NULL,
		Name NVARCHAR(50),
		CONSTRAINT PK_Products PRIMARY KEY CLUSTERED (Id)
    );

IF NOT EXISTS (SELECT * FROM sysobjects WHERE NAME='Categories' and xtype='U')
    CREATE TABLE Categories (
        Id INT IDENTITY NOT NULL,
		Name NVARCHAR(50),
		CONSTRAINT PK_Categories PRIMARY KEY CLUSTERED (Id)
    );

IF NOT EXISTS (SELECT * FROM sysobjects WHERE NAME='ProductCategories' and xtype='U')
    CREATE TABLE ProductCategories (
		ProductId INT,
		CategoryId INT,
		CONSTRAINT PK_ProductCategories PRIMARY KEY CLUSTERED (ProductId, CategoryId),
		CONSTRAINT FK_ProductCategories_Products FOREIGN KEY (ProductId) REFERENCES Products(Id),
		CONSTRAINT FK_ProductCategories_Categoris FOREIGN KEY (CategoryId) REFERENCES Categories(Id)
    );

    IF (SELECT COUNT(*) 
          FROM Products) = 0
INSERT INTO Products(Name)
SELECT 'Product ' + CAST(ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS NVARCHAR(50)) AS ProductName
  FROM generate_series(1, 30);

    IF (select COUNT(*) 
          FROM Categories) = 0
INSERT INTO Categories(Name)
SELECT 'Category ' + cast(row_number() OVER (ORDER BY (SELECT 1)) AS NVARCHAR(50)) AS CategoryName
  FROM generate_series(1, 5);

    IF (SELECT COUNT(*) 
          FROM ProductCategories) = 0
INSERT INTO ProductCategories(ProductId, CategoryId)
SELECT DISTINCT 
       ABS(CHECKSUM(NewId())) % 30 + 1 AS RandomProduct,
       ABS(CHECKSUM(NewId())) % 5 + 1  AS RandomCategory
  FROM generate_series(1, 40);

SELECT p.Name AS ProductName,
       c.Name AS CategoryName
  FROM Products               AS p
  LEFT JOIN ProductCategories AS pc
    ON p.Id = pc.ProductId
  LEFT JOIN Categories        AS c
    ON c.Id = pc.CategoryId;