﻿using AreaCalculator.Abstractions;

namespace AreaCalculator
{
    public class Triangle : FigureBase
    {
        private double _firstSide;
        private double _secondSide;
        private double _thirdSide;

        /// <summary>
        /// Первая сторона треугольника.
        /// </summary>
        public double FirstSide
        {
            get
            {
                return _firstSide;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Сторона треугольника не может быть меньше нуля.");
                }

                _firstSide = value;
            }
        }

        /// <summary>
        /// Вторая сторона треугольника.
        /// </summary>
        public double SecondSide
        {
            get
            {
                return _secondSide;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Сторона треугольника не может быть меньше нуля.");
                }

                _secondSide = value;
            }
        }

        /// <summary>
        /// Третья сторона треугольника.
        /// </summary>
        public double ThirdSide
        {
            get
            {
                return _thirdSide;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Сторона треугольника не может быть меньше нуля.");
                }

                _thirdSide = value;
            }
        }

        /// <summary>
        /// Создание экземпляра треугольника по трем сторонам.
        /// </summary>
        public Triangle(double firstSide = default, double secondSide = default, double thirdSide = default)
        {
            FirstSide = firstSide;
            SecondSide = secondSide;
            ThirdSide = thirdSide;
        }

        /// <summary>
        /// Метод расчета площади треугольника.
        /// </summary>
        /// <returns>Площадь треугольника.</returns>
        protected override double CalculateFigureArea()
        {
            if (!IsExists)
            {
                throw new InvalidOperationException("Треугольник с заданными сторонами не существует.");
            }

            var halfPerimeter = (_firstSide + _secondSide + _thirdSide) / 2;
            var area = Math.Sqrt(
                halfPerimeter *
                (halfPerimeter - _firstSide) *
                (halfPerimeter - _secondSide) *
                (halfPerimeter - _thirdSide));
            return area;
        }

        /// <summary>
        /// Существует ли треугольник с заданными сторонами.
        /// </summary>
        public bool IsExists
        {
            get
            {
                double[] sides = { _firstSide, _secondSide, _thirdSide };
                bool noZeroSides = !sides.Any(x => x == 0);

                if (!noZeroSides)
                {
                    return false;
                }

                Array.Sort(sides);

                return sides[2] <= sides[0] + sides[1];
            }
        }

        /// <summary>
        /// Прямоугольный ли треугольник.
        /// </summary>
        public bool IsRight
        {
            get
            {
                if (!IsExists)
                {
                    throw new InvalidOperationException("Треугольник с заданными сторонами не существует.");
                }

                double[] sides = { _firstSide, _secondSide, _thirdSide };
                Array.Sort(sides);

                return Math.Pow(sides[2], 2) == Math.Pow(sides[0], 2) + Math.Pow(sides[1], 2);
            }
        }
    }
}