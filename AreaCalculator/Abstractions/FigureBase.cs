﻿namespace AreaCalculator.Abstractions
{
    public abstract class FigureBase : IFigure
    {
        /// <summary>
        /// Площадь фигуры.
        /// </summary>
        public double Area
        {
            get 
            {
                return CalculateFigureArea();
            }
        }

        /// <summary>
        /// Метод расчета площади фигуры. Должен быть реализован в классе-наследнике.
        /// </summary>
        /// <returns>Площадь фигуры.</returns>
        protected abstract double CalculateFigureArea();
    }
}