﻿namespace AreaCalculator.Abstractions
{
    public interface IFigure
    {
        /// <summary>
        /// Площадь фигуры.
        /// </summary>
        public double Area { get; }
    }
}