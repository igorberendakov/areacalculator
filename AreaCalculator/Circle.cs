﻿using AreaCalculator.Abstractions;

namespace AreaCalculator
{
    public class Circle : FigureBase
    {
        private double _radius;

        /// <summary>
        /// Радиус окружности.
        /// </summary>
        public double Radius
        {
            get
            {
                return _radius;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Радиус не может быть меньше нуля.");
                }

                _radius = value;
            }
        }

        /// <summary>
        /// Создание экземпляра окружности по радиусу.
        /// </summary>
        /// <param name="radius"></param>
        public Circle(double radius = default)
        {
            Radius = radius;
        }

        /// <summary>
        /// Метод расчета площади круга.
        /// </summary>
        /// <returns>Площадь круга.</returns>
        protected override double CalculateFigureArea()
        {
            var area = Math.PI * Math.Pow(Radius, 2);
            return area;
        }
    }
}
