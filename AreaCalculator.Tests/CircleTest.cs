using AreaCalculator.Abstractions;

namespace AreaCalculator.Tests
{
    public class CircleTest
    {
        [Fact]
        public void AreaTest()
        {
            //Arrange
            var radius = 10d;
            IFigure circle = new Circle(radius);

            //Act
            var result = circle.Area;

            //Assert
            Assert.Equal(314.1592653589793d, result);
        }

        [Fact]
        public void NegativeRadiusTest()
        {
            //Arrange
            var radius = -10d;

            //Act
            var action = () => new Circle(radius);
            var secondAction = () => new Circle { Radius = radius };

            //Assert
            Assert.Throws<ArgumentException>(action);
            Assert.Throws<ArgumentException>(secondAction);
        }

        [Fact]
        public void ZeroRadiusAreaTest()
        {
            //Arrange
            IFigure circle = new Circle();

            //Act
            var result = circle.Area;

            //Assert
            Assert.Equal(0d, result);
        }
    }
}