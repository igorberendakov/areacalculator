using AreaCalculator.Abstractions;

namespace AreaCalculator.Tests
{
    public class TriangleTest
    {
        [Fact]
        public void AreaTest()
        {
            //Arrange
            var firstSide = 5d;
            var secondSide = 3d;
            var thirdSide = 4d;
            IFigure figure = new Triangle(firstSide, secondSide, thirdSide);
            var expectedArea = 6d;

            //Act
            var result = figure.Area;

            //Assert
            Assert.Equal(expectedArea, result);
        }

        [Fact]
        public void NegativeSideTest()
        {
            //Arrange
            var firstSide = -10d;

            //Act
            var createTriangleWithNegativeSide = () => new Triangle(firstSide);
            var createTriangleWithNegativeSideProperty = () => new Triangle { FirstSide = firstSide };

            //Assert
            Assert.Throws<ArgumentException>(createTriangleWithNegativeSide);
            Assert.Throws<ArgumentException>(createTriangleWithNegativeSideProperty);
        }

        [Fact]
        public void ZeroSidesAreaTest()
        {
            //Arrange
            IFigure figure = new Triangle();

            //Act
            var getArea = () => { var area = figure.Area; };

            //Assert
            Assert.Throws<InvalidOperationException>(getArea);
        }

        [Fact]
        public void IsTriangleRightTest()
        {
            //Arrange
            var notRightTriangle = new Triangle(1, 2, 2);
            var zeroSidesTriangle = new Triangle();
            var rightTriangle = new Triangle(4, 3, 5);
            var notExistingTriangle = new Triangle(1, 2, 5);

            //Act
            var getIsRightZeroSidedTriangle = () => { var variable = notExistingTriangle.IsRight; };
            var getIsRightOnNotExistingTriangle = () => { var variable = notExistingTriangle.IsRight; };

            //Assert
            Assert.False(notRightTriangle.IsRight);
            Assert.Throws<InvalidOperationException>(getIsRightZeroSidedTriangle);
            Assert.True(rightTriangle.IsRight);
            Assert.Throws<InvalidOperationException>(getIsRightOnNotExistingTriangle);
        }

        [Fact]
        public void IsTriangleExistsTest()
        {
            //Arrange
            var existingTriangle = new Triangle(1, 2, 3);
            var zeroSidesTriangle = new Triangle();
            var notExistingTriangle = new Triangle(1, 5, 2);

            //Assert
            Assert.True(existingTriangle.IsExists);
            Assert.False(zeroSidesTriangle.IsExists);
            Assert.False(notExistingTriangle.IsExists);
        }
    }
}